import { Validator } from 'vee-validate';

// https://baianat.github.io/vee-validate/guide/localization.html#localization-api
// Vee-validate dictionary for field errors.
const dictionary = {
    custom: {
        activeMilitaryMember: {
            required: 'We cannot offer you a loan if you are a regular or reserve member of the US military'
        },
        email: {
            required: 'Email address is not specified.',
            email: 'Email address is invalid.',
        },
        first_name: {
            required: 'First name is not specified.',
            alpha_spaces: 'First name can only consist of letters and spaces.',
        },
        last_name: {
            required: 'Last name is not specified.',
            alpha_spaces: 'Last name can only consist of letters and spaces.',
        },
        phone_number: {
            required: 'Phone number is not specified.',
            min: 'Phone number must consist of at least 10 characters.',
            max: 'Phone number must consist of less than 11 characters.',
        },
        state: {
            required: 'Please select a state.',
        },
        termsAndConditions: {
            required: "You must agree to our terms and conditions in order to continue"
        },
    },
};

// https://baianat.github.io/vee-validate/guide/localization.html#aliases
Validator.localize('en', dictionary);

// exporting module
export default dictionary;