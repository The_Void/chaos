import Vue from 'vue'
import Vuex from 'vuex'
import {user} from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
    // https://vuex.vuejs.org/guide/modules.html
    // Defining/declaring modules in store
    modules: {
        user
    }
})
