// Function that will be used to reset the state
let initialState = () => {
    return { 
        first_name: '',
        last_name: '',
        phone_number: '',
        email: '',
        state: 'CO',
        termsAndConditions: '',
        personalInformation: '',
        loan_number: '',
     }
}; 
// User module
export const user = {
    // https://vuex.vuejs.org/guide/modules.html#namespacing
    // Allows module to be more self-contained and/or reuseable
    namespaced: true,
    // https://vuex.vuejs.org/guide/state.html#state
    state: initialState,
    // https://vuex.vuejs.org/guide/mutations.html#mutations
    mutations: {
        UPDATE_FIRST_NAME(state, payload) {
            state.first_name = payload;
        },
        UPDATE_LAST_NAME(state, payload) {
            state.last_name = payload;
        },
        UPDATE_PHONE_NUMBER(state, payload) {
            state.phone_number = payload;
        },
        UPDATE_EMAIL(state, payload) {
            state.email = payload;
        },
        UPDATE_STATE(state, payload) {
            state.state = payload;
        },
        UPDATE_ACTIVE_MILITARY_MEMBER(state, payload) {
            state.activeMilitaryMember = payload;
        },
        UPDATE_TERMS_AND_CONDITIONS(state, payload) {
            state.termsAndConditions = payload;
        },
        UPDATE_PERSONAL_INFORMATION(state, payload) {
            state.personalInformation = payload;
        },
        UPDATE_LOAN_NUMBER(state, payload) {
            state.loan_number = payload;
        },
        RESET_STATE(state) {
            // acquire initial state
            const clonedState = initialState();
            
            Object.keys(clonedState).forEach(key => {
                state[key] = clonedState[key]
            });
        }
    },
}