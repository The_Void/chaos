import axios from 'axios';

//Configuring axios defaults
axios.defaults.baseURL = process.env.VUE_APP_API_URL;
// axios.defaults.headers.common['X-Window-Location'] = window.location.href;
// axios.defaults.headers.common['X-Document-Referrer'] = document.referrer;
axios.defaults.headers.common['Accept'] = 'application/json; charset=utf-8';
axios.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';

export default axios;