import axios from './config'

export default {
    apiPost(payload) {
        return axios({
            // HTTP method POST
            method: "post",
            // API endpoint
            url: '/submitInitialSubmission',
            // Data being sent to API
            data: {
                'firstName': payload.first_name,
                'lastName': payload.last_name,
                'mobilePhone': payload.phone_number,
                'email': payload.email,
                'state': payload.state,

            }
        })
            .then(response => {
                return response;
            })
            .catch(error => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);

                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
            }) 

    },
    getUsers() {
        return axios({
            // HTTP method POST
            method: "get",
            // API endpoint
            url: '/users',
        })
            .then(response => {
                return response;
            })
            .catch(error => {
                if (error.response) {
                    // The request was made and the server responded with a status code
                    // that falls out of the range of 2xx
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);

                } else if (error.request) {
                    // The request was made but no response was received
                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                    // http.ClientRequest in node.js
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }
            }) 
    }
}
