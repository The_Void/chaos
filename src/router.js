import Vue from "vue";
import Router from "vue-router";
import Door_1 from "./views/Door_1.vue";
import Door_2 from "./views/Door_2.vue";
import Door_3 from "./views/Door_3.vue";
import Door_4 from "./views/Door_4.vue";

Vue.use(Router);

export default new Router({
  mode: "abstract",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "door_1",
      component: Door_1
    },
    {
      path: "/door_2",
      name: "door_2",
      component: Door_2
    },
    {
      path: "/door_3",
      name: "door_3",
      component: Door_3
    },
    {
      path: "/door_4",
      name: "door_4",
      component: Door_4
    },
  ]
});
