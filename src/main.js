import Vue from "vue";
import App from "./App.vue";
// Vue router
import router from "./router";
// Vuex
import store from "./store/store";
// Bootstrap
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
// VeeValidate
import VeeValidate from 'vee-validate'
import Dictionary from './validation.js';
// Input plugin
import VueTheMask from 'vue-the-mask'
// Animation library
import Animate from 'animate.css';
// Cookie library
import VueCookie from 'vue-cookie';
// https://github.com/FortAwesome/vue-fontawesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSpinner, faUser, faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


Vue.component('font-awesome-icon', FontAwesomeIcon);
// https://vuejs.org/v2/api/#Vue-use
Vue.use(VueCookie);

Vue.use(Animate);

Vue.use(VeeValidate, {
  dictionary: Dictionary
});

Vue.use(VueTheMask);

// Font Awesome spinner
library.add(faSpinner, faUser, faPhone, faEnvelope);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  mounted() {
    this.$router.replace("/");
  }
}).$mount("#app");
