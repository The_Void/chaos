// Used for the build/production application
// Appends forward slash to url
// Builds out files without hashes
module.exports = {
    publicPath: '/',
    filenameHashing: false
}